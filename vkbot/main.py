from server import Server
from utils import read_to_dict

NAME = 'name'
TOKEN = 'api_token'
GROUP = 'group_id'

SETTINGS_PATH = 'data/settings'


def server_manager():
    settings = read_to_dict(SETTINGS_PATH)
    bot_server = Server(settings[TOKEN], settings[GROUP], settings[NAME])
    bot_server.test()
    bot_server.start()


if __name__ == '__main__':
    server_manager()
