

def read_to_list(filename: str, low=False):
    with open(filename) as f:
        scan_list = [s for s in f.read().splitlines()]
    if low:
        scan_list = list(map(str.lower, scan_list))
    return scan_list


def read_to_dict(filename: str):
    scan_dict = {}
    with open(filename) as f:
        lines = f.read().splitlines()
        scan_dict.update([s.split(':') for s in lines])
    return scan_dict
