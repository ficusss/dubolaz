import vk_api
import re
# import logging
# import logging.config
import traceback

from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from random import randint


class Server:
    def __init__(self, api_token, group_id, server_name: str = "Empty"):

        # logging.config.fileConfig('data/logging.conf')
        # self.logger = logging.getLogger('dubolaz')
        # self.logger.info('Logger is started.')

        # Даем серверу имя
        # self.logger.info('Устанавливаем имя сервера: ' + server_name)
        self.server_name = server_name

        # Для использования Long Poll API
        # self.logger.info('Настраиваем соединение с ВКонтакте.')
        self.vk = vk_api.VkApi(token=api_token)
        self.long_poll = VkBotLongPoll(self.vk, group_id)

        # Для вызова методов vk_api
        self.vk_api = self.vk.get_api()

    def test(self):
        # Посылаем сообщение пользователю с указанным ID
        self.send_msg(16576437, "Привет-привет!")

    def send_msg(self, send_id, message):
        """
        Отправка сообщения через метод messages.send
        :param send_id: vk id пользователя, который получит сообщение
        :param message: содержимое отправляемого письма
        :return: None
        """
        self.vk_api.messages.send(peer_id=send_id,
                                  random_id=randint(0, 10000000),
                                  message=message)

    def start(self):
        try:
            for event in self.long_poll.listen():  # Слушаем сервер
                # Зписываем в логфайл
                self.write_to_logfile(event)

                # Пришло новое сообщение
                if event.type == VkBotEventType.MESSAGE_NEW:
                    # self.logger.info('Новое сообщение является текстовым')
                    self.incoming_message_handler(event)

        except Exception:
            # self.logger.info("Ой-ой, кажется что-то пошло не так. Вот ошибка:")
            # self.logger.error(traceback.format_exc())
            msg = "HELP! I crashed.\n\n" + traceback.format_exc()
            self.send_msg(16576437, msg)
        finally:
            # self.logger.info("Выключаюсь... До свидания!")
            self.send_msg(16576437, "Выключаюсь... До свидания!")

    def write_to_logfile(self, event):
        pass
        # self.logger.info(f"Полученное новое сообщение.\n {event}")

    def incoming_message_handler(self, event):
        """ Обработчик пришедшего сообщения"""
        text = event.object.text
        reg = re.compile('[^a-zA-Zа-яА-Я ]')
        text = reg.sub('', text).lower()
        is_other = True

        if is_other:
            # self.logger.info('Текст не опознан!')
            username = self.get_user_name(event.object.from_id)
            self.send_msg(event.object.peer_id, f"{username}, я получил ваше сообщение!")

    def get_user_name(self, user_id):
        """ Получаем имя пользователя"""
        return self.vk_api.users.get(user_id=user_id)[0]['first_name']

    def get_user_city(self, user_id):
        """ Получаем город пользователя"""
        return self.vk_api.users.get(user_id=user_id, fields="city")[0]["city"]['title']
