from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import ugettext as _
from django.urls import reverse
from django.db import models


class Tree(models.Model):
    """A model representing a physical apparently climbable tree."""

    # Fields
    specie = models.OneToOneField('Specie', models.CASCADE,
                                  help_text='Tree''s species.',
                                  blank=True)
    # If city is destroyed, it's very likely that the tree is vaporized as well
    city = models.OneToOneField('City', models.CASCADE,
                                help_text='City where this tree grows')
    lattitude = models.DecimalField(help_text='Lattitude',
                                    max_digits=8, decimal_places=6)
    longitude = models.DecimalField(help_text='Longigude',
                                    max_digits=9, decimal_places=6)
    creator = models.OneToOneField('User', models.SET_NULL, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of Tree."""
        return reverse('tree', args=[str(self.id)])

    def __str__(self):
        """String for representing the Tree object (in Admin site etc.)."""
        return ''


class User(models.Model):
    """A model representing a drevolaz aka user."""

    # Fields
    name = models.CharField(max_length=24, help_text='Drevolaz name',
                            unique=True)
    password = models.CharField(max_length=32, help_text='Password')
    vk = models.CharField(max_length=64, help_text='VK id', unique=True)
    city = models.OneToOneField('City', models.SET_NULL, null=True,
                                help_text='City where drevolaz lives')
    email = models.EmailField(help_text='E-mail', unique=True)
    trees = models.IntegerField(help_text='Amount of trees drevolaz created',
                                default=0, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-trees']

    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of Tree."""
        return reverse('climber', args=[str(self.id)])

    def __str__(self):
        return '{} ({})'.format(self.name, self.id)


class Country(models.Model):
    """A model representing a country."""

    # Fields
    name = models.CharField(max_length=24, help_text='Country name',
                            unique=True)

    def __str__(self):
        return _('{}'.format(self.name))


class City(models.Model):
    """A model representing a city."""

    # Fields
    name = models.CharField(max_length=86, help_text='City name')
    # If country dies, then cities die with it.
    country = models.OneToOneField('Country', models.CASCADE)

    def __str__(self):
        return _('{}'.format(self.name))


class Specie(models.Model):
    """A model representing a species' name."""

    # Fields
    name = models.CharField(max_length=24, help_text='Specie name',
                            unique=True)

    def __str__(self):
        return _('{}'.format(self.name))


class Opinion(models.Model):
    """A model representing a drevolazes opinion on a tree"""

    # Fields
    tree = models.OneToOneField('Tree', models.CASCADE,
                                help_text='Rated tree')
    creator = models.OneToOneField('User', models.CASCADE, null=True)
    climbability = models.IntegerField(validators=[MinValueValidator(0),
                                                   MaxValueValidator(5)])
    description = models.TextField(blank=True,
                                   help_text='Drevolazes overall experience')
    creation_date = models.DateTimeField(auto_now_add=True)

    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of Tree."""
        return reverse('opinion', args=[str(self.id)])

    def __str__(self):
        """String for representing the Tree object (in Admin site etc.)."""
        return ''
