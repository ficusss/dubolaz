from django.shortcuts import render
from django.utils.translation import ugettext as _
from .models import User
import random

def slogan():
    strs = ['Дуб - ты мир!', 'Покорабаемся?', 'Деревья...',
               'Дуболаз дуболазу дуболаз', 'И ИВАМИ РОДИМЫМИ!..']
    return _(random.choice(strs))
    

def index(request):
    context = {'title': _('Дуболазанье'),
               'msg': slogan(),
               'active_index': 'active'}
    return render(request, 'index.html', context)

def users(request):
    context = {'title': _('Дуболазы'),
               'users': User.objects.all(),
               'active_users': 'active'}
    return render(request, 'users.html', context)