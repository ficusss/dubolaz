from catalog.models import Country, City, Specie, User, Tree, Opinion
from django.contrib import admin

items = [Country, City, Specie, User, Tree, Opinion]
for _ in map(admin.site.register, items):
	pass
